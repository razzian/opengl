/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: Modele.cpp                                                         * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 
#include "Modele.h"

Modele::Modele(u_int32_t largeurFenetre, u_int32_t hauteurFenetre) :
        mAngleRotation(3.0) ,
        x(0.0) ,
        y(0.0) ,
        up(false) ,
        down(false) ,
        right(false) ,
        left(false) ,
        plus(false) ,
        minus(false) ,
        Z(false) ,
        Q(false) ,
        S(false) ,
        D(false) ,
        // Construction de la caméra :
        mCamera (50.0f,(float)mLargeurFenetre/(float)mHauteurFenetre, 0.01f, 10000.0f, 0.1f, 0.015f, //perspective
                 50.0f, 0.0f, 0.0f, //Position
                 0.0f, 0.0f, 0.0f, //Focus
                 0.0f, 1.0f, 0.0f), //Verticale
        scene(),
        mLargeurFenetre(largeurFenetre),
        mHauteurFenetre(hauteurFenetre),
        mDisplayManager() // Construction du gestionnaire de vue
{
  // initialisation OpenGL
  glEnable(GL_NORMALIZE);

  // Initialistaion de la scene et des paramètres OpenGL
  scene = InitialiserScene::initDepuisFichier("Objet3D/apple2.3DS");		// permet d'initialiser la scene depuis un fichier exterieur
  //scene = InitialiserScene::init();								// permet de charger une scene predefini manuellement
}

const Camera &Modele::getCamera() const {
  return mCamera;
}

void Modele::ApplyModelTransform() {
  translation();												
  rotation();
  mCamera.ApplyCameraCoordinates();									// application des transformations à la camera
}

void Modele::translation() {

  vector<GLfloat> tmpDeplacement = {0.0, 0.0, 0.0};								// par défaut pas de translation
  if(up)																		// si la touche up est enfoncée
  {
    tmpDeplacement[1] = tmpDeplacement[1] + mCamera.getVitesseTranslation();	// on avance de la vitesse de translation
  }
  if(down)																		// si la touche down est enfoncée
  {
    tmpDeplacement[1] = tmpDeplacement[1] - mCamera.getVitesseTranslation();	// on recule de la vitesse de transaltion
  }
  if(right)
  {
    tmpDeplacement[0] = tmpDeplacement[0] - mCamera.getVitesseTranslation();
  }
  if(left)
  {
    tmpDeplacement[0] = tmpDeplacement[0] + mCamera.getVitesseTranslation();
  }
  if(plus)
  {
    tmpDeplacement[2] = tmpDeplacement[2] + mCamera.getVitesseTranslation();
  }
  if(minus)
  {
    tmpDeplacement[2] = tmpDeplacement[2] - mCamera.getVitesseTranslation();
  }
  mCamera.Translation(tmpDeplacement);											// on transmet la translation calculée à la caméra
}

void Modele::rotation() {
  vector<GLfloat> angle = {0.0, 0.0};											// par défaut pas de rotation
  if(Z)																			// si la touche Z est enfoncée
  {
    angle[1] = angle[1] - mCamera.getVitesseRotation();							// on fait une rotation positive
  }
  if(S)																			// si la touche S est enfoncée
  {
    angle[1] = angle[1] + mCamera.getVitesseRotation();							// on fait une rotation négative
  }
  if(Q)
  {
    angle[0] = angle[0] + mCamera.getVitesseRotation();
  }
  if(D)
  {
    angle[0] = angle[0] - mCamera.getVitesseRotation();
  }
  mCamera.Rotation(angle);														// on transmet la roataion calculé à la caméra
}

void Modele::UpdateUp(bool b)
{
  up = b;
}

void Modele::UpdateDown(bool b)
{
  down = b;
}

void Modele::UpdateRight(bool b)
{
  right = b;
}

void Modele::UpdateLeft(bool b)
{
  left = b;
}

void Modele::UpdatePlus(bool b)
{
  plus = b;
}

void Modele::UpdateMinus(bool b)
{
  minus = b;
}
void Modele::UpdateZ(bool b)
{
  Z = b;
}
void Modele::UpdateQ(bool b)
{
  Q = b;
}
void Modele::UpdateS(bool b)
{
  S = b;
}
void Modele::UpdateD(bool b)
{
  D = b;
}

u_int32_t Modele::getLargeurFenetre() const {
  return mLargeurFenetre;
}

u_int32_t Modele::getHauteurFenetre() const {
  return mHauteurFenetre;
}

void Modele::Update() {
  mDisplayManager.Affichage(*this);									// Mise à jour de la vue
}

void Modele::UpdateMouseMotion(int xShift, int yShift) {
  x = xShift;
  y = yShift;
}

void Modele::Redimensionnement(u_int32_t largeurFenetre, u_int32_t hauteurFenetre) {
  mLargeurFenetre = largeurFenetre;
  mHauteurFenetre = hauteurFenetre;
  // On modifie l'ASPECT la camera au cas ou l/h est changé 
  mCamera.UpdateAspect((float)largeurFenetre/(float)hauteurFenetre); // nouvel aspect
  mDisplayManager.setWindowChanged();
}


void Modele::setScene(GraphicObject & newScene)
{
  scene = newScene ;
}

void Modele::dessiner() const
{
  scene.dessiner();													// on dessine la scene
}

