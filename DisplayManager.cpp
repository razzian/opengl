/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: DisplayManager.cpp                                                 * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#include "Modele.h"
#include "DisplayManager.h"


DisplayManager::DisplayManager():
  mWindowChanged(true)
{
  FramesData::Init();

  // initialisation openGL
  glClearColor(1.0,1.0,1.0,1.0);
  glColor3f(0.0, 0.0, 0.0);
  glEnable(GL_DEPTH_TEST);
  // initialisation des textures
  TextureManager::activerTexture();
  // initialisation de l'eclairage
  Eclairage::activerEclairage();
}

void DisplayManager::setWindowChanged() {
  mWindowChanged = true;
}

void DisplayManager::Affichage( Modele& modele){
  // Si les dimensions de la fenêtre ont changé (ou à l'initialisation)
  if (mWindowChanged){
    Redimensionnement(modele); // (re-)Initialisation de la projection 3D --> 2D
    mWindowChanged = false;
  }
	// On efface les buffers (écran et profondeur)
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  // On se place dans le repère de monde
  Camera::ClearModelView();
  // On met à jour notre modéle
  modele.ApplyModelTransform();
  // On active le ou les eclairages voulus
  Eclairage::initEclairageDiffus();
  Eclairage::initEclairageSpeculaire();
  Eclairage::initEclairageAmbiant();
  // On dessine notre modéle
  modele.dessiner();
}

void DisplayManager::Redimensionnement(const Modele& modele){
  // On recadre la fenêtre centrée en (0, 0) aux bonnes dimensions
  GeometricTransform::Viewport(0, 0, (GLsizei) modele.getLargeurFenetre(), (GLsizei) modele.getHauteurFenetre());
  // On redéfinit la projection en perspective
  Camera::ClearProjection();
  modele.getCamera().ApplyPerspectiveProjection();
}
