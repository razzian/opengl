# TP OpenGL  


## Comment installer l'application pour le développement  

L'application ne nécessite pas plus d'installation que un clone du dépôt git et les outils présents sur les machines virtuelles de l'ISIMA.  
Les bibliothèques nécessaires sont :  
1. OpenGL  version 3.3
2. La SDL 2  


## Ce qui a été réalisé

1. Il est possible de charger une scène soit en créant soi-même les objets 3D soit en chargeant un objet avec ASSIMP.
2. Il est possible de déplacer la caméra dans la scène (rotation avec les touches z,q,sd et translation avec les touches haut, bas, gauche, droite, - et +  sur tous les axes).
3. Il est possible d'éclairer la scène avec un éclairage ambiant et/ou diffus et/ou spéculaire. Intialement la couleur de l'éclairage ambiant est le gris, pour les autres éclairages la couleur est le violet.
4. Il a été tenté d'ajouter des textures.


## Ce qui n'a pas été réalisé

1. Nous avons essayé d'appliquer des textures sur les objets. Les textures semblent bien se charger mais elle n'apparait pas sur l'objet. L'objet reste noir. Il n'y a pas de message d'erreur. Après plusieurs recherches, nous n'avons pas réussi à identifier la source du problème.
