/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: Modele.h                                                           * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#ifndef MODELE_FLMISTRAL
#define MODELE_FLMISTRAL

#include <cstdlib>
#include <vector>

#include "Camera.h"
#include "GraphicObject.h"
#include "InitialiserScene.h"
#include "Eclairage.h"
#include "GeometricTransform.h"
#include "DisplayManager.h"


using namespace std;


/**
 * \class Modele Modele.h
 * \brief Class Modele
 *
 * Classe représentant le modele, donc les données. C'est le modèle dans le sens d'e la partie modèle d'un MVC. 
 */
class Modele {

  double mAngleRotation;						/*!<  Angle de rotation pour l'animation */
  double x;										/*!< position de la souris dans la fenetre sur l'axe des x */
  double y;										/*!< position de la souris dans la fenetre sur l'axe des y */
  bool up;										/*!<  indique si la touche up du clavier est enfoncée */
  bool down;									/*!<  indique si la touche down du clavier est enfoncée */
  bool right;									/*!<  indique si la touche right du clavier est enfoncée */
  bool left;									/*!<  indique si la touche left du clavier est enfoncée */
  bool plus;									/*!<  indique si la touche + du clavier est enfoncée */
  bool minus;									/*!<  indique si la touche - du clavier est enfoncée */			
  bool Z;										/*!<  indique si la touche z du clavier est enfoncée */
  bool Q;										/*!<  indique si la touche q du clavier est enfoncée */
  bool S;										/*!<  indique si la touche s du clavier est enfoncée */
  bool D;										/*!<  indique si la touche d du clavier est enfoncée */

  Camera mCamera;								/*!<  caméra qui observera la scène */
  GraphicObject scene;							/*!<  objet graphique qui contient tous les objets de la scène. */

  u_int32_t mLargeurFenetre;					/*!<  largeur de la fenêtre graphique en pixels */
  u_int32_t mHauteurFenetre;					/*!<  hauteur de la fenêtre graphique en pixels */
  DisplayManager mDisplayManager;				/*!<  Gestionnaire de la vue (affichage) */

public:

	/**
	* \fn sCamera(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne,
	* \brief  Constructeur prenant en paramètre la géométrie de la fenêtre.
	*
	* Initialise les données de l'application et données nécessaires à l'affichage.
	*
	* \param largeurFenetre largeur de la fenêtre graphique en pixels
	* \param hauteurFenetre hauteur de la fenêtre graphique en pixels
	*/
	Modele(u_int32_t largeurFenetre, u_int32_t hauteurFenetre);

	/**
	* \fn u_int32_t getLargeurFenetre() const
	* \brief getter de mLargeurFenetre
	*
	* \return l'attribut mLargeurFenetre du modele
	*/
	u_int32_t getLargeurFenetre() const;

	/**
	* \fn u_int32_t getHauteurFenetre() const
	* \brief getter de mHauteurFenetre
	*
	* \return l'attribut mHauteurFenetre du modele
	*/
	u_int32_t getHauteurFenetre() const;

	/**
	* \fn const Camera &getCamera() const
	* \brief getter de mHauteurFela camera
	*
	* \return une référence de l'attribut mCamera du modele
	*/
	const Camera &getCamera() const;


	/**
	* \fn ApplyModelTransform()
	* \brief Applique la transformation du modèle
	*/
	void ApplyModelTransform();


	/**
	* \fn void Update()
	* \brief Mise à jour du modèle invoquée à chaque événement timer
	*/
	void Update();

	/**
	* \fn void UpdateMouseMotion(int xShift, int yShift)
	* \brief Mise à jour du modèle invoquée à chaque événement souris
	*
	* \param xShift Variation de la coordonnée x de la souris
	* \param yShift Variation de la coordonnée y de la souris
	*/
	void UpdateMouseMotion(int xShift, int yShift);

	/**
	* \fn void UpdateUp(bool b)
	* \brief Mise à jour de l'attribut Up invoqué à chaque événement de la touche up
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateUp(bool b);

	/**
	* \fn void UpdateDown(bool b)
	* \brief Mise à jour l'attribut down invoqué à chaque événement de la touche down
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateDown(bool b);

	/**
	* \fn void UpdateRight(bool b)
	* \brief Mise à jour l'attribut right invoqué à chaque événement de la touche right
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateRight(bool b);

	/**
	* \fn void UpdateLeft(bool b)
	* \brief Mise à jour l'attribut left invoqué à chaque événement de la touche left
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateLeft(bool b);

	/**
	* \fn void UpdatePlus(bool b)
	* \brief Mise à jour l'attribut plus invoqué à chaque événement de la touche +
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdatePlus(bool b);

	/**
	* \fn void UpdateMinus(bool b)
	* \brief Mise à jour l'attribut minus invoqué à chaque événement de la touche -
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateMinus(bool b);

	/**
	* \fn void UpdateZ(bool b)
	* \brief Mise à jour l'attribut Z invoqué à chaque événement de la touche z
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateZ(bool b);

	/**
	* \fn void UpdateQ(bool b)
	* \brief Mise à jour l'attribut Q invoqué à chaque événement de la touche q
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateQ(bool b);

	/**
	* \fn void UpdateS(bool b)
	* \brief Mise à jour l'attribut S invoqué à chaque événement de la touche s
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateS(bool b);

	/**
	* \fn void UpdateD(bool b)
	* \brief Mise à jour l'attribut D invoqué à chaque événement de la touche d
	*
	* \param b Indication si la touche a été enfoncée ou relâchée
	*/
	void UpdateD(bool b);

	/**
	* \fn void translation()
	* \brief Applique une translation a la camera
	*/
	void translation();

	/**
	* \fn void rotation()
	* \brief Applique une rotation a la camera
	*/
	void rotation();


	/**
	* \fn void Redimensionnement(u_int32_t largeurFenetre, u_int32_t hauteurFenetre)
	* \brief  Réglage du cadrage pour la vue
	*
	* Doit être rappelée si la taille de la vue change (SDL_WINDOWEVENT)
	*
	* \param largeurFenetre largeur de la fenêtre graphique en pixels
	* \param hauteurFenetre hauteur de la fenêtre graphique en pixels
	*/
	void Redimensionnement(u_int32_t largeurFenetre, u_int32_t hauteurFenetre);


	/**
	* \fn void setScene(GraphicObject & newScene)
	* \brief setter de scene
	*
	* \param newScene la nouvelle valeur de l'attribut scene du modele
	*/
	void setScene(GraphicObject & newScene);

	/**
	* \fn void dessiner() const
	* \brief dessine la scene du modele
	*/
	void dessiner() const;
};
#endif
