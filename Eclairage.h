/**
 * \file Eclairage.h
 * \brief Fichier d'entête de la classe Eclairage
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe Eclairage
 *
 */
#ifndef ECLAIRAGE_FLMISTRAL
#define ECLAIRAGE_FLMISTRAL

#include <GL/glut.h>

/**
 * \class Eclairage Eclairage.h
 * \brief Class Eclairage
 *
 * Classe permettant d'initialiser les différents types d'eclairages que l'on souhaites dans notre scéne 
 */
class Eclairage
{
    public :

        /**
        * \fn static void initEclairageAmbiant()
        * \brief active un eclairage ambiant gris
        */
        static void initEclairageAmbiant();

        /**
        * \fn static void initEclairageDiffus()
        * \brief active un eclairage diffus violet avec pour position  x=75, y=75, z=0 
        */
        static void initEclairageDiffus();

        /**
        * \fn static void initEclairageSpeculaire()
        * \brief active un eclairage speculaire violet avec pour position  x=75, y=75, z=0 
        */
        static void initEclairageSpeculaire();

        /**
        * \fn static void activerEclairage()
        * \brief initialisation qui active la possibilite de faire de l'eclairage par la suite
        */
        static void activerEclairage();
};

#endif