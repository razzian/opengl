################################################################################
#     Copyright (C) 2017 by Rémy Malgouyres                                    # 
#     http://malgouyres.org                                                    # 
#                                                                              # 
# The program is distributed under the terms of the GNU General Public License # 
#                                                                              # 
################################################################################ 

CXXFLAGS =  -std=c++11 -Wall -pedantic \
    -pedantic-errors -Wextra -Wcast-align \
    -Wcast-qual  -Wchar-subscripts  -Wcomment -Wconversion \
    -Wdisabled-optimization \
    -Werror -Wformat -Wformat=2 \
    -Wformat-nonliteral -Wformat-security  \
    -Wformat-y2k \
    -Wimport  -Winit-self  -Winline \
    -Winvalid-pch   \
    -Wunsafe-loop-optimizations -Wmissing-braces \
    -Wmissing-field-initializers -Wmissing-format-attribute   \
    -Wmissing-include-dirs -Wmissing-noreturn \
    -Wparentheses  -Wpointer-arith \
    -Wredundant-decls -Wreturn-type \
    -Wsequence-point  -Wshadow -Wsign-compare  -Wstack-protector \
    -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch  -Wswitch-default \
    -Wswitch-enum -Wtrigraphs  -Wuninitialized \
    -Wunknown-pragmas  -Wunreachable-code -Wunused \
    -Wunused-function  -Wunused-label  -Wunused-parameter \
    -Wunused-value  -Wunused-variable  -Wvariadic-macros \
    -Wvolatile-register-var  -Wwrite-strings -Wfloat-equal \
    -Weffc++


all: runDemo

runDemo: main.cpp GeometricTransform.cpp DisplayManager.cpp EventController.cpp FramesData.cpp  Modele.cpp \
         WrapperSDL.cpp MouseData.cpp MainApplication.cpp WindowManager.cpp Camera.cpp GraphicObject.cpp Sommet.cpp Face.cpp InitialiserScene.cpp Eclairage.cpp\
         TextureManager.cpp VecMath.cpp\
				GeometricTransform.h DisplayManager.h EventController.h FramesData.h  Modele.h\
         WrapperSDL.h MouseData.cpp MainApplication.h WindowManager.h Camera.h  GraphicObject.h Sommet.h Face.h InitialiserScene.h Eclairage.h\
         TextureManager.h VecMath.h

	g++ $(CXXFLAGS) main.cpp GeometricTransform.cpp DisplayManager.cpp EventController.cpp FramesData.cpp  Modele.cpp\
         WrapperSDL.cpp MouseData.cpp MainApplication.cpp WindowManager.cpp Camera.cpp GraphicObject.cpp Sommet.cpp Face.cpp InitialiserScene.cpp Eclairage.cpp\
         TextureManager.cpp VecMath.cpp\
				 -o runDemo -lSDL2 -lGL -lGLU  -lSDL2_ttf -lglut -lassimp
clean:
	rm -f runDemo
