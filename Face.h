/**
 * \file Face.h
 * \brief Fichier d'entête de la classe Face
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe Face
 *
 */
#ifndef FACE_H
#define FACE_H

#include "Sommet.h"

/**
 * \class Face Face.h
 * \brief Class Face
 *
 * Classe representant une face triangulaire dans un repere en 3d 
 */
class Face
{
    public :

    Sommet s1; /*!< premier des trois sommets du triangle representant la face */
    Sommet s2; /*!< deuxième des trois sommets du triangle representant la face */
    Sommet s3; /*!< troisième des trois sommets du triangle representant la face */
  
  	/**
	* \fn Face(Sommet, Sommet, Sommet)
	* \brief Constructeur d'instance de Face.
	*
	* \param ps1 premier des trois sommets du triangle representant la face
	* \param ps2 deuxième des trois sommets du triangle representant la face
	* \param ps3 troisième des trois sommets du triangle representant la face
	*/
    Face(Sommet ps1, Sommet ps2, Sommet ps3);
};


#endif