/**
 * \file Sommet.h
 * \brief Fichier d'ent�te de la classe Sommet
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'ent�te de la classe Sommet
 *
 */

#ifndef SOMMET_FLMISTRAL
#define SOMMET_FLMISTRAL

#include <GL/glut.h>
#include <math.h>
#include <limits>
#include <vector>


 /**
  * \class Sommet Sommet.h
  * \brief Class Sommet
  *
  * Classe repr�sentant un sommet dans un espace 3D.
  */
class Sommet
{


    public :

    GLfloat x;								/*!< position selon l'axde des x */
    GLfloat y;								/*!< position selon l'axde des y */
    GLfloat z;								/*!< position selon l'axde des z */
    std::vector<GLfloat> normal;			/*!< vecteur normal au sommet */

	/**
	* \fn Sommet(GLfloat px, GLfloat py, GLfloat pz)
	* \brief constructeur de la classe avec trois GLfloat
	*
	* \param px la position selon l'axe des x
	* \param py la position selon l'axe des y
	* \param pz la position selon l'axe des z
	*/
    Sommet(GLfloat px, GLfloat py, GLfloat pz);

	/**
	* \fn Sommet(GLfloat px, GLfloat py, GLfloat pz, std::vector<GLfloat> pnormal)
	* \brief constructeur de la classe avec trois GLfloat et un vecteur normal
	*
	* \param px la position selon l'axe des x
	* \param py la position selon l'axe des y
	* \param pz la position selon l'axe des z
	* \param pnormal le vecteur normal au sommet
	*/
    Sommet(GLfloat px, GLfloat py, GLfloat pz, std::vector<GLfloat> pnormal);

	/**
	* \fn Sommet(Sommet const & s)
	* \brief constructeur de copie de la calsse Sommet
	*
	* \param s le sommet � copier
	*/
    Sommet(Sommet const & s);


	/**
	* \fn bool operator==(Sommet s)
	* \brief surcharge de l'operateur =
	*
	* \param s le sommet � comparer
	*
	* \return vrai si les deux sommets sont �gaux faut sinon
	*/
    bool operator==(Sommet);

};


#endif