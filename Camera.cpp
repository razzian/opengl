#include "Camera.h"
Camera::Camera(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne, GLfloat vitesseTranslation,GLfloat vitesseRotation, GLfloat positionX, GLfloat positionY, GLfloat positionZ,
	GLfloat pointDeViseeX, GLfloat pointDeViseeY, GLfloat pointDeViseeZ, GLfloat vecteurVerticalX, GLfloat vecteurVerticalY, GLfloat vecteurVerticalZ) :
	mAngleOuvertureY(angleOuvertureY), mAspect(aspect), mz_proche(z_proche), mz_eloigne(z_eloigne), mVitesseTranslation(vitesseTranslation), mVitesseRotation(vitesseRotation), mPosition() ,mPointDeVisee(),mVecteurVertical() 
{
	mPosition.push_back(positionX);
	mPosition.push_back(positionY);
	mPosition.push_back(positionZ);

	mPointDeVisee.push_back(pointDeViseeX);
	mPointDeVisee.push_back(pointDeViseeY);
	mPointDeVisee.push_back(pointDeViseeZ);

	mVecteurVertical.push_back(vecteurVerticalX);
	mVecteurVertical.push_back(vecteurVerticalY);
	mVecteurVertical.push_back(vecteurVerticalZ);
}

GLfloat Camera::getVitesseTranslation() const
{
	return(mVitesseTranslation);
}

void Camera::setVitesseTranslation(GLfloat vitesseTranslation)
{
	mVitesseTranslation = vitesseTranslation;
}

GLfloat Camera::getVitesseRotation() const
{
	return(mVitesseRotation);
}

void Camera::setVitesseRotation(GLfloat vitesseRotation)
{
	mVitesseRotation = vitesseRotation;
}


void Camera::setProjection(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne)
{
	mAngleOuvertureY = angleOuvertureY;
	mAspect = aspect;
	mz_proche = z_proche;
	mz_eloigne = z_eloigne;
}

void Camera::LookAt(vector<GLfloat> position, vector<GLfloat> pointDeVisee, vector<GLfloat> vecteurVertical)
{
	mPosition = position;
	mPointDeVisee = pointDeVisee;
	mVecteurVertical = vecteurVertical;
}

void Camera::UpdateAspect(GLfloat aspect)
{
	setProjection(mAngleOuvertureY, aspect, mz_proche, mz_eloigne);
}

void Camera::ApplyPerspectiveProjection() const
{
	GeometricTransform::ApplyPerspectiveProjection(mAngleOuvertureY, mAspect, mz_proche, mz_eloigne);
}

void Camera::ClearModelView()
{
	GeometricTransform::ClearModelView();
}

void Camera::ClearProjection()
{
	GeometricTransform::ClearProjection();
}

void Camera::ApplyCameraCoordinates() const
{
	GeometricTransform::LookAt(mPosition, mPointDeVisee, mVecteurVertical);
}

void Camera::Translation(vector<GLfloat> deplacement)
{
	// on calcule les 3 vecteurs unitairs de la base de la caméra
	vector<GLfloat> V = VecMath::normalise(mVecteurVertical);
	vector<GLfloat> D = VecMath::normalise(VecMath::vecteur(mPosition,mPointDeVisee));
	vector<GLfloat> A = VecMath::normalise(VecMath::produitVectoriel(V,D)); 

	// on calcule les 3 déplacements à appliquer à la caméra par rapport à la base x,y,z du monde
	GLfloat deplacementX = deplacement[0]*A[0] + deplacement[1]*V[0] + deplacement[2]*D[0];
	GLfloat deplacementY = deplacement[0]*A[1] + deplacement[1]*V[1] + deplacement[2]*D[1];
	GLfloat deplacementZ = deplacement[0]*A[2] + deplacement[1]*V[2] + deplacement[2]*D[2];

	// on applique aux coordonnées de la caméra ces deplacements
	mPosition[0] = mPosition[0] + deplacementX;
	mPosition[1] = mPosition[1] + deplacementY;
	mPosition[2] = mPosition[2] + deplacementZ;

	mPointDeVisee[0] = mPointDeVisee[0] + deplacementX;
	mPointDeVisee[1] = mPointDeVisee[1] + deplacementY;
	mPointDeVisee[2] = mPointDeVisee[2] + deplacementZ;
}

void Camera::Rotation(vector<GLfloat> angles)
{
	// on calcule le vecteur vertical unitairse de la base de la caméra
	vector<GLfloat> V = VecMath::normalise(mVecteurVertical); 
	// on calcule le vecteur direction de la base de la caméra
	vector<GLfloat> D = VecMath::vecteur(mPosition,mPointDeVisee);
	// on calcule le troisième vecteur unitaire de la base de la caméra
	vector<GLfloat> A = VecMath::normalise(VecMath::produitVectoriel(V,D));

	
	// angles[0] => rotation autour de V
	if( (angles[0] > 0.001) || (angles[0] < -0.001) )
	{
		// on applique la rotation d'angle angles[0] autour de V aux coordonnées de la caméra
		vector<GLfloat> NouvD1 = VecMath::rotationVectorielle(V,D,angles[0]);

		mPointDeVisee[0] = mPosition[0] + NouvD1[0];
		mPointDeVisee[1] = mPosition[1] + NouvD1[1];
		mPointDeVisee[2] = mPosition[2] + NouvD1[2];
	}

	// angles[1] => rotation autour de A
	if( (angles[1] > 0.001) || (angles[1] < -0.001) )
	{
		// on applique la rotation d'angle angles[1] autour de A aux coordonnées de la caméra
		vector<GLfloat> NouvD2 = VecMath::rotationVectorielle(A,D,angles[1]);

		mVecteurVertical = VecMath::rotationVectorielle(A,mVecteurVertical,angles[1]);
		mPointDeVisee[0] = mPosition[0] + NouvD2[0];
		mPointDeVisee[1] = mPosition[1] + NouvD2[1];
		mPointDeVisee[2] = mPosition[2] + NouvD2[2];
	}
}