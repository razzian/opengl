/**
 * \file VecMath.h
 * \brief Fichier d'entête de la classe VecMath
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe VecMath
 *
 */
#ifndef VEC_MATH_FLMISTRAL
#define VEC_MATH_FLMISTRAL

#include <vector>
#include <GL/glut.h>
#include <math.h>
#include <limits>
#include <cmath>

#include "Sommet.h"

using namespace std;

/**
 * \class VecMath VecMath.h
 * \brief Class VecMath
 *
 * Classe proposant des méthodes pour le calcul vectoriel dans un espace en 3d
 */
class VecMath
{
    public :
    
		/**
		* \fn static vector<GLfloat> vecteur(Sommet const & a, Sommet const & b)
		* \brief Renvoie le vecteur ab avec a et b les deux points entrés en parèmétre
		*
		* \param a Le sommet d'origine du vecteur
		* \param b Le sommet final du vecteur
		*
		* \return le vecteur de GLfloat ab de taille 3
		*/
        static vector<GLfloat> vecteur(Sommet const & a, Sommet const & b);

		/**
		* \fn static vector<GLfloat> vecteur(vector<GLfloat> const & a, vector<GLfloat> const & b)
		* \brief Renvoie le vecteur ab avec a et b les deux points dont les coordonées sont entrées en parèmétre
		*
		* \param a Les coordonnées du sommet d'origine du vecteur
		* \param b Les coordonnées du sommet final du vecteur
		*
		* \return le vecteur de GLfloat ab de taille 3
		*/
        static vector<GLfloat> vecteur(vector<GLfloat> const & a, vector<GLfloat> const & b);

		/**
		* \fn static vector<GLfloat> normalise(vector<GLfloat> const & v)
		* \brief Renvoie le vecteur entré en parmètre après normalisation
		*
		* \param v Le vecteur de GLfloat de taille 3 à normaliser
		*
		* \return le vecteur de GLfloat de taille nomalisé
		*/
        static vector<GLfloat> normalise(vector<GLfloat> const & v);

		/**
		* \fn static vector<GLfloat> produitVectoriel(vector<GLfloat> u, vector<GLfloat> v)
		* \brief Renvoie le résultat du produit vectoriel des deux vecteurs entrés en parmètre
		*
        * \param u Le premier vecteur du produit vectoriel
		* \param v Le second vecteur du produit vectoriel
		*
		* \return le vecteur résultant du produit vectoriel
		*/
        static vector<GLfloat> produitVectoriel(vector<GLfloat> u, vector<GLfloat> v);

		/**
		* \fn static GLfloat produitScalaire(vector<GLfloat> u, vector<GLfloat> v)
		* \brief Renvoie le résultat du produit sclaire des deux vecteurs entrés en parmètre
		*
        * \param u Le premier vecteur du produit scalaire
		* \param v Le second vecteur du produit scalaire
		*
		* \return le scalaire(Glfloat) résultant du produit scalaire
		*/
        static GLfloat produitScalaire(vector<GLfloat> u, vector<GLfloat> v);

		/**
		* \fn static vector<GLfloat> produitParScalaire(vector<GLfloat> u, GLfloat k)
		* \brief Renvoie le résultat du produit d'un vecteurs et d'un scalair entrés en parmètre
		*
        * \param u Le vecteur à multiplier
		* \param k Le scalaire qui va multplier le vecteur
		*
		* \return le vecteur resultant du produit.
		*/
        static vector<GLfloat> produitParScalaire(vector<GLfloat> u, GLfloat k);

		/**
		* \fn static vector<GLfloat> sommeVecteur(vector<GLfloat> u, vector<GLfloat> v)
		* \brief Renvoie le résultat de la somme des deux vecteurs entrés en paramètre
		*
        * \param u Le premier vecteur de la somme
		* \param v Le second vecteur de la somme
		*
		* \return le vecteur resultant de la somme.
		*/
        static vector<GLfloat> sommeVecteur(vector<GLfloat> u, vector<GLfloat> v);

        /**
		* \fn static vector<GLfloat> sommeVecteur(vector<GLfloat> u, vector<GLfloat> v, vector<GLfloat> w)
		* \brief Renvoie le résultat de la somme des trois vecteurs entrés en paramètre
		*
        * \param u Le premier vecteur de la somme
		* \param v Le deuxième vecteur de la somme
		* \param w Le troisième vecteur de la somme
		*
		* \return le vecteur resultant de la somme.
		*/
        static vector<GLfloat> sommeVecteur(vector<GLfloat> u, vector<GLfloat> v, vector<GLfloat> w);

        /**
		* \fn static vector<GLfloat> vecteurNormal(Sommet const & a, Sommet const & b, Sommet const & c)
		* \brief Renvoie le vecteur normal au plan défini par les trois sommets entrés en paramétre
		*
        * \param a Le premier sommet définissant la face
		* \param b Le deuxième sommet définissant la face
		* \param b Le troisième sommet définissant la face
		*
		* \return le vecteur normal à la face
		*/
        static vector<GLfloat> vecteurNormal(Sommet const & a, Sommet const & b, Sommet const & c);

        /**
		* \fn static vector<GLfloat> rotationVectorielle(vector<GLfloat> N, vector<GLfloat> U, GLfloat theta)
		* \brief Renvoie une copie du vecteur U aprés avoir subis une rotation d'angle theta autour de l'axe qui a pour vecteur unitaire N
		*
        * \param N Le vecteur unitaire de l'axe de rotation
		* \param U Le vecteur au qu'elle on veut appliquer une rotation
		* \param theta L'angle de la rotation à effectuer
		*
		* \return Le vecteur qui a subi la rotation
		*/
        static vector<GLfloat> rotationVectorielle(vector<GLfloat> N, vector<GLfloat> U, GLfloat theta);
};

#endif