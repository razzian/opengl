/**
 * \file GraphicObject.h
 * \brief Fichier d'entête de la classe GraphicObject
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe GraphicObject
 *
 */
#ifndef GRAPHIC_OBJECT_FLMISTRAL
#define GRAPHIC_OBJECT_FLMISTRAL

#include <cstdlib>
#include <vector>
#include <GL/glut.h>
#include <math.h>
#include <limits>

#include "Sommet.h"
#include "Face.h"
#include "TextureManager.h"
#include "VecMath.h"

using namespace std;

/**
 * \class GraphicObject GraphicObject.h
 * \brief Class GraphicObject
 *
 * Classe representant un objet graphique.
 * Un objet graphique est caractérisé par une liste de faces triangulaires.
 * Il peut également être compose d'autres objets grahiques .
 */
class GraphicObject
{
    int nbSommet;                               /*!< Nombre de sommet de l'objet graphique*/
    int nbFace;                                 /*!< Nombre de face composant l'objet graphique*/
    vector<Sommet> sommets;                     /*!< La liste des sommets de l'objet graphique*/
    vector<Face> faces;                         /*!< La liste des faces de l'objet graphique */
    vector<GraphicObject> objetGraphiques;      /*!< La liste des autres objects graphiques inclut dans l'objet graphique */
    GLuint texture;                             /*!< La texture à appliqué sur l'objet graphique */

    public :

	/**
	* \fn GraphicObject()
	* \brief Constructeur par defaut d'instance de GraphicObject.
	*/
    GraphicObject();

    /**
	* \fn ~GraphicObject()
	* \brief Destructeur par defaut d'instance de GraphicObject.
	*/
    ~GraphicObject();

	/**
	* \fn int getnbFace()
	* \brief getter de nbFace
	*
	* \return L'attribut nbFace du GraphicObject
	*/
    int getnbFace(); 

    /**
	* \fn void setTexture(const char * image)
	* \brief setter de texture
    *
    * \param image La nouvelle valeur de l'attribut texture du GraphicObject
	*/
    void setTexture(const char * image);

    /**
	* \fn void ajouterSommet(GLfloat x,GLfloat y,GLfloat z)
	* \brief Ajoute un sommet à la liste des sommet du GraphicObject.
    *
    * \param x Coordonnee x du sommet à ajouter.
    * \param y Coordonnee y du sommet à ajouter.
    * \param z Coordonnee z du sommet à ajouter.
	*/
    void ajouterSommet(GLfloat x,GLfloat y,GLfloat z);

    /**
	* \fn void ajouterSommet(Sommet s)
	* \brief Ajoute un sommet à la liste des sommet du GraphicObject.
    *
    * \param s Sommet à ajouter
	*/
    void ajouterSommet(Sommet s);

    /**
	* \fn void ajouterObjetGraphique(GraphicObject & go)
	* \brief Ajoute un objet graphique a la liste des objets graphiques du GraphicObject.
    *
    * \param go référence du GraphicObject a ajouter
	*/
    void ajouterObjetGraphique(GraphicObject & go);

    /**
	* \fn bool chercherSommet(Sommet s)
	* \brief Permet de savoir si un sommet est present dans le GraphicObject (si il est dans sa liste de sommet).
    *
    * \param s Le sommet dont on veut vérifier sa presence dans le GraphicObject.
    *
    * \return un booléen contenant vrai si le sommet a ete trouve et faux sinon
	*/
    bool chercherSommet(Sommet s);

    /**
	* \fn void definirFace(Sommet s1, Sommet s2, Sommet s3)
	* \brief Ajoute une face à notre GraphicObject
    *
    * \param s1 Le premier sommet de la face à ajouter, il sera ajouter à la liste des sommets si il n'y est pas déjà.
    * \param s2 Le deuxième sommet de la face à ajouter, il sera ajouter à la liste des sommets si il n'y est pas déjà.
    * \param s3 Le troisième sommet de la face à ajouter, il sera ajouter à la liste des sommets si il n'y est pas déjà.
	*/
    void definirFace(Sommet s1, Sommet s2, Sommet s3);

    /**
	* \fn void dessiner() const
	* \brief Construit l'objet graphique ainsi que les objets graphiques le composants pour être afficher par openGL. 
	*/
    void dessiner() const;






};


#endif
