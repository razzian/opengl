#include "Sommet.h"

Sommet::Sommet(GLfloat px, GLfloat py, GLfloat pz) : x(px) , y(py) ,z(pz), normal({0.0,0.0,0.0})
{}

Sommet::Sommet(GLfloat px, GLfloat py, GLfloat pz, std::vector<GLfloat> pnormal) : x(px) , y(py) ,z(pz), normal(pnormal)
{}

Sommet::Sommet(Sommet const & s ) : x(s.x) , y(s.y) , z(s.z), normal (s.normal)
{}

bool Sommet::operator==(Sommet s)
{
    GLfloat epsilon = std::numeric_limits<GLfloat>::epsilon();

    return((fabs(x - s.x) < epsilon) && (fabs(y - s.y) < epsilon) && (fabs(z - s.z) < epsilon));		// deux sommets sont �gaux si leurs trois coordonn�es le sont
}

