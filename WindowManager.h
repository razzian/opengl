#ifndef WINDOW_MANAGER_FLMISTRAL
#define WINDOW_MANAGER_FLMISTRAL

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GLES3/gl3.h>


/** @brief Gestionnaire de la fenêtre graphique et du contexte OpenGL
   */
class WindowManager{

  public:
    /** @brief Fonction d'initialisation de la fenêtre SDL
      @param windowWidth largeur de la fenêtre en pixels
      @param windowWidth largeur de la fenêtre en pixels
      @param windowTitle Titre de la fenêtre dans sa barre de titre
    */
    WindowManager(int largeurFenetreInit, int hauteurFenetreInit,
                  const char* windowTitle);

    SDL_Window* getP_Window() const;

    /** Destruction : Libération des ressources SDL et OpenGL */
    ~WindowManager();

  private:
    /** Adresse de la fenêtre SDL */
    SDL_Window *mP_Window;
    /** Adresse du contexte OpenGL */
    SDL_GLContext *mP_GlContext;

    /** @brief Fonction d'initialisation de la fenêtre SDL
      @param windowWidth largeur de la fenêtre en pixels
      @param windowWidth largeur de la fenêtre en pixels
      @param windowTitle Titre de la fenêtre dans sa barre de titre
    */
    static SDL_Window * init_SDL_Window(int windowWidth, int windowHeight, const char* windowTitle);

    // On interdit explicitement la recopie
    WindowManager(const WindowManager&) = delete;
    WindowManager & operator=(const WindowManager&) = delete;
};

#endif