/**
 * \file TextureManager.h
 * \brief Fichier d'ent�te de la classe textureManager
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'ent�te de la classe textureManager
 *
 */

#ifndef HEADER_TEXTUREMANAGER_FLMISTRAL
#define HEADER_TEXTUREMANAGER_FLMISTRAL

#include <GL/glut.h>
#include <fstream>


 /**
  * \class textureManager TextureManager.h
  * \brief Class textureManager
  *
  * Classe qui a � charge la gestion des textures. Elle active les textures et permet de charger une texute � partir d'un fichier .bmp
  */
class TextureManager
{

	public :


		/**
		* \fn static GLuint telechargerTexture(const char * image)
		* \brief La fonction charge une image pour en faire une texture.
		* 
		* Cette fonction est inspir�e de celle propos�e dans le tuto : http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-5-a-textured-cube/
		*
		* \param image Chemin d'acc�s � la texture au format BMP
		*
		* \return identifiant de la texture charg�e
		*/
		static GLuint telechargerTexture(const char * image);


		/**
		* \fn static void activerTexture();
		* \brief permet d'activer l'option texture dans OpenGL
		*/
		static void activerTexture();

};

#endif
