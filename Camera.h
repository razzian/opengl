/**
 * \file Camera.h
 * \brief Fichier d'entête de la classe Camera
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe Camera
 *
 */
#ifndef CAMERA_FLMISTRAL
#define CAMERA_FLMISTRAL

#include <GL/glut.h>
#include <vector>
#include <cstring>

#include "VecMath.h"
#include "Eclairage.h"
#include "GeometricTransform.h"

using namespace std;

/**
 * \class Camera Camera.h
 * \brief Class Camera
 *
 * Classe representant la camera avec son positionnement dans le repere de la scene 
 * et des methodes pour modifier ce positionnement.
 */
class Camera {
	GLfloat mAngleOuvertureY; 			/*!< Angle d'ouverture de la camera */
	GLfloat mAspect;					/*!< Ratio largeur sur hauteur de la fenetre vue par la camera */
	GLfloat mz_proche;					/*!< Distance à partir de laquelle les objets sont visibles par la camera */
	GLfloat mz_eloigne;					/*!< Distance à partir de laquelle les objets ne sont plus visibles par la camera */
	GLfloat mVitesseTranslation;		/*!< Vitesse appliquee lorsque l'on fait une translation de la caméra */
	GLfloat mVitesseRotation;			/*!< Vitesse appliquee lorsque l'on fait une rotation de la caméra */
	vector<GLfloat> mPosition;			/*!< Coordonnées de la camera */
	vector<GLfloat> mPointDeVisee;		/*!< Coordonnées du point visée par la caméra */
	vector<GLfloat> mVecteurVertical;	/*!< Coordonnées du vecteur qui est vertical dans le plan visé par la caméra */

public:
	/**
	* \fn Camera(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne,
	*			  GLfloat vitesseTranslation,GLfloat vitesseRotation,
	*			  GLfloat positionX, GLfloat positionY, GLfloat positionZ,
	*         	  GLfloat pointDeViseeX, GLfloat pointDeViseeY, GLfloat pointDeViseeZ,
	*			  GLfloat vecteurVerticalX, GLfloat vecteurVerticalY, GLfloat vecteurVerticalZ)
	* \brief Constructeur d'instance de Camera.
	*
	* \param angleOuvertureY Angle d'ouverture de la camera en GLfloat
	* \param aspect Ratio largeur sur hauteur de la fenétre vue par la caméra en GLfloat
	* \param z_proche Distance à partir de laquelle les objets sont visibles par la caméra en GLfloat
	* \param z_eloigne Distance à partir de laquelle les objets ne sont plus visibles par la caméra en GLfloat
	* \param vitesseTranslation Vitesse appliquee lorsque l'on fait une translation de la caméra en GLfloat
	* \param vitesseRotation Vitesse appliquee lorsque l'on fait une rotation de la camera en GLfloat
	* \param positionX Coordonnee x de la camera en GLfloat
	* \param positionY Coordonnee y de la camera en GLfloat
	* \param positionZ Coordonnee z de la camera en GLfloat
	* \param pointDeViseeX Coordonnee x du point visée par la camera en GLfloat
	* \param pointDeViseeY Coordonnee y du point visée par la camera en GLfloat
	* \param pointDeViseeZ Coordonnee z du point visée par la camera en GLfloat
	* \param vecteurVerticalX Coordonnee x du vecteur qui est vertical dans le plan vise par la caméra en GLfloat
	* \param vecteurVerticalY Coordonnee y du vecteur qui est vertical dans le plan vise par la caméra en GLfloat
	* \param vecteurVerticalZ Coordonnee z du vecteur qui est vertical dans le plan vise par la caméra en GLfloat
	*/
	Camera(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne,
		   GLfloat vitesseTranslation,GLfloat vitesseRotation,
		   GLfloat positionX, GLfloat positionY, GLfloat positionZ,
		   GLfloat pointDeViseeX, GLfloat pointDeViseeY, GLfloat pointDeViseeZ,
		   GLfloat vecteurVerticalX, GLfloat vecteurVerticalY, GLfloat vecteurVerticalZ);

	/**
	* \fn GLfloat getVitesseTranslation() const
	* \brief getter de mVitesseTranslation
	*
	* \return l'attribut mVitesseTranslation de la Camera
	*/
	GLfloat getVitesseTranslation() const;

	/**
	* \fn void setVitesseTranslation(GLfloat vitesseTranslation)
	* \brief setter de mVitesseTranslation
	*
	* \param vitesseTranslation la nouvelle valeur de l'attribut mVitesseTranslation de la Camera
	*/
	void setVitesseTranslation(GLfloat vitesseTranslation);

	/**
	* \fn GLfloat getVitesseRotation() const
	* \brief getter de mvitesseRotation
	*
	* \return l'attribut mvitesseRotation de la Camera
	*/
	GLfloat getVitesseRotation() const;

	/**
	* \fn void setVitesseRotation(GLfloat vitesseRotation)
	* \brief setter de mvitesseRotation
	*
	* \param vitesseRotation la nouvelle valeur de l'attribut mvitesseRotation de la Camera
	*/
	void setVitesseRotation(GLfloat vitesseRotation);

	/**
	* \fn void setProjection(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne)
	* \brief setter d'une nouvelle projection de la Camera
	*
	* \param angleOuvertureY la nouvelle valeur de l'attribut mAngleOuvertureY de la Camera
	* \param aspect la nouvelle valeur de l'attribut mAspect de la Camera
	* \param z_proche la nouvelle valeur de l'attribut mz_proche de la Camera
	* \param z_eloigne la nouvelle valeur de l'attribut mz_eloigne de la Camera
	*/
	void setProjection(GLfloat angleOuvertureY, GLfloat aspect, GLfloat z_proche, GLfloat z_eloigne);

	/**
	* \fn void LookAt(vector<GLfloat> position, vector<GLfloat> pointDeVisee, vector<GLfloat> vecteurVertical)
	* \brief setter d'un nouveau trio de coordonnées de la Camera
	*
	* \param position la nouvelle valeur de l'attribut mPosition de la Camera
	* \param pointDeVisee la nouvelle valeur de l'attribut mPointDeVisee de la Camera
	* \param vecteurVertical la nouvelle valeur de l'attribut mVecteurVertical de la Camera
	*/
	void LookAt(vector<GLfloat> position, vector<GLfloat> pointDeVisee, vector<GLfloat> vecteurVertical);

	/**
	* \fn void UpdateAspect(GLfloat aspect)
	* \brief setter de mAspect
	*
	* \param aspect la nouvelle valeur de l'attribut mAspect de la Camera
	*/
	void UpdateAspect(GLfloat aspect);

	/**
	* \fn void ApplyPerspectiveProjection() const
	* \brief applique la projection de la Camera avec ces attributs actuels
	*/
	void ApplyPerspectiveProjection() const;

	/**
	* \fn static void ClearModelView()
	* \brief passe la matrice GL_MODELVIEW comme matrice courante 
	*/
	static void ClearModelView();

	/**
	* \fn static void ClearProjection()
	* \brief passe la matrice GL_PROJECTION comme matrice courante 
	*/
	static void ClearProjection();

	/**
	* \fn void ApplyCameraCoordinates() const
	* \brief applique le trio de coordonnées de la Camera avec ces attributs actuels
	*/
	void ApplyCameraCoordinates() const;

	/**
	* \fn void Translation(vector<GLfloat> deplacement)
	* \brief translate la caméra selon le deplacement demandé
	*
	* \param deplacement Un vecteur de 3 GLfloat contenant les valeurs de déplacement horizontal, vertical et de zomm de la Caméra
	*/
	void Translation(vector<GLfloat> deplacement);

	/**
	* \fn void Rotation(vector<GLfloat> angles)
	* \brief rotatione la caméra selon les angles demandés
	*
	* \param angles Un vecteur de 2 GLfloat contenant les angles de rotation horizontal et vertical de la Caméra
	*/
	void Rotation(vector<GLfloat> angles);
};

#endif

