#include "TextureManager.h"

GLuint TextureManager::telechargerTexture(const char * image )
{
	// Information a lire dans le header de l'image
	unsigned char header[54]; 
	unsigned int dataPos;     
	unsigned int largeur;
	unsigned int hauteur;
	unsigned int tailleImage; 
	unsigned char * data;
	
	GLuint texture;
	
	FILE * fichier = fopen(image,"r");																			// on ouvre le fichier
	
	if( !fichier )																								// si le fichier ne s'ouvre pas
	{
		texture = 0;
	}
	else																										// si le fichier s'est ouvert
	{
		if( (fread(header, 1, 54, fichier)!=54) && ( header[0]!='B' || header[1]!='M' ) )						// on lit l'en-tete si sa taille n'est pas de 54, l'en-tete commence par BM il y a un probleme avec l'image
		{ 
			texture = 0 ;
		}	
		else
		{
			dataPos = *(int*)&(header[0x0A]);
			tailleImage = *(int*)&(header[0x22]);
			largeur = *(int*)&(header[0x12]);
			hauteur = *(int*)&(header[0x16]);
			
			// si les informations sont manquantes on complete manuellement
			if (tailleImage==0)    tailleImage=largeur*hauteur*3; 
			if (dataPos==0)      dataPos=54; 
			
			data = new unsigned char [tailleImage];																// creation du buffer
			fread(data,1,tailleImage,fichier);																	// copie des infos dans le buffer
			
			glGenTextures(1, &texture);																			// genere un identifiant de texture qui est place dans texture
			glBindTexture(GL_TEXTURE_2D, texture);																// active la texture
			glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, largeur, hauteur, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
		}
		fclose(fichier);																						// fermeture du fichier
	}
	return texture;
}


void TextureManager::activerTexture()
{
	glEnable(GL_TEXTURE_2D);
}
