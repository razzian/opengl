#include "GeometricTransform.h"

void GeometricTransform::Viewport(int32_t viewCenterX, int32_t viewCenterY, u_int32_t viewWidth, u_int32_t viewHeight)
{
	glViewport(viewCenterX, viewCenterY, viewWidth, viewHeight);
}

void GeometricTransform::ApplyPerspectiveProjection(GLfloat angleOuvertureY, GLfloat aspect, GLfloat zProche, GLfloat zEloigne)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(angleOuvertureY,aspect, zProche, zEloigne);
}

void GeometricTransform::ClearModelView()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GeometricTransform::ClearProjection()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

void GeometricTransform::LookAt(const vector<GLfloat> position, const vector<GLfloat> pointDeVisee, const vector<GLfloat> vecteurVertical)
{
	gluLookAt(position[0], position[1], position[2], pointDeVisee[0], pointDeVisee[1], pointDeVisee[2], vecteurVertical[0], vecteurVertical[1], vecteurVertical[2]);
}

void GeometricTransform::Translate(GLfloat vecX, GLfloat vecY, GLfloat vecZ)
{
	glTranslated(vecX, vecY, vecZ);
}

void GeometricTransform::Rotate(GLfloat vecX, GLfloat vecY, GLfloat vecZ, GLfloat angle)
{
	glRotated(vecX, vecY, vecZ, angle);
}

void GeometricTransform::scale(GLfloat factorX, GLfloat factorY, GLfloat factorZ)
{
	glScaled(factorX, factorY, factorZ);
}