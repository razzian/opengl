#include "VecMath.h"


vector<GLfloat> VecMath::vecteur(Sommet const & a, Sommet const & b) 
{
    vector<GLfloat> retour;
    retour.push_back(b.x - a.x);
    retour.push_back(b.y - a.y);
    retour.push_back(b.z - a.z);
    return retour;
}

vector<GLfloat> VecMath::vecteur(vector<GLfloat> const & a, vector<GLfloat> const & b)
{
    vector<GLfloat> retour;
    retour.push_back(b[0] - a[0]);
    retour.push_back(b[1] - a[1]);
    retour.push_back(b[2] - a[2]);
    return retour;
}

vector<GLfloat> VecMath::normalise(vector<GLfloat> const & v)
{
    vector<GLfloat> retour;
    // on calcul la norme du vecteur
    GLfloat norm = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    if(norm >0.0001f) //si la norm n'est pas proche de 0
    {   // on normalise le vecteur
        retour.push_back(v[0] / norm);
        retour.push_back(v[1] / norm);
        retour.push_back(v[2] / norm);
    }
    else    // sinon
    {   // on retourne le vecteur nul
        retour = {0,0,0};
    }
    return retour;
}

vector<GLfloat> VecMath::produitVectoriel(vector<GLfloat> u, vector<GLfloat> v)
{
    vector<GLfloat> retour;
    retour.push_back(u[1] * v[2] - u[2] * v[1]);
    retour.push_back(u[2] * v[0] - u[0] * v[2]);
    retour.push_back(u[0] * v[1] - u[1] * v[0]);
    return retour;
}

GLfloat VecMath::produitScalaire(vector<GLfloat> u, vector<GLfloat> v)
{
    GLfloat res;
    for(unsigned int i=0; i< u.size(); i++)
    { 
            res = res + u[i] * v[i];
    }
    return res;
}

vector<GLfloat> VecMath::produitParScalaire(vector<GLfloat> u, GLfloat k)
{
    vector<GLfloat> retour;
    retour.push_back(u[0] * k);
    retour.push_back(u[1] * k);
    retour.push_back(u[2] * k);
    return retour;
}

vector<GLfloat> VecMath::sommeVecteur(vector<GLfloat> u, vector<GLfloat> v)
{
    vector<GLfloat> retour;
    retour.push_back(u[0] + v[0]);
    retour.push_back(u[1] + v[1]);
    retour.push_back(u[2] + v[2]);
    return retour;
}

vector<GLfloat> VecMath::sommeVecteur(vector<GLfloat> u, vector<GLfloat> v, vector<GLfloat> w)
{
    vector<GLfloat> retour;
    retour.push_back(u[0] + v[0] + w[0]);
    retour.push_back(u[1] + v[1] + w[1]);
    retour.push_back(u[2] + v[2] + w[2]);
    return retour;
}

vector<GLfloat> VecMath::vecteurNormal(Sommet const & a, Sommet const & b, Sommet const & c)
{
    vector<GLfloat> ab = vecteur(a,b);
    vector<GLfloat> ac = vecteur(a,c);
    return produitVectoriel(ab,ac);
}

vector<GLfloat> VecMath::rotationVectorielle(vector<GLfloat> N, vector<GLfloat> U, GLfloat theta)
{
    GLfloat cos_theta = cos(theta);
    GLfloat sin_theta = sin(theta);

    vector<GLfloat> V = sommeVecteur(produitParScalaire(U, cos_theta), produitParScalaire(produitVectoriel( N,  U), sin_theta), produitParScalaire(produitParScalaire(N,  produitScalaire( N,  U)), (1 - cos_theta)));
    return V;
}

