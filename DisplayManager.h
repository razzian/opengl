/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: DisplayManager.h                                                   * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#ifndef DISPLAY_MANAGER_FLMISTRAL
#define DISPLAY_MANAGER_FLMISTRAL

#include <GLES3/gl3.h>
#include <GL/glut.h>

#include "FramesData.h"
#include "GeometricTransform.h"
#include "TextureManager.h"
#include "Eclairage.h"


class Modele;

/**
 * CLASSE DE GESTION DE L'AFFICHAGE (accède au modèle et affiche les données)
 */
class DisplayManager{
  /** true si les dimensions de la fenêtre ont changé.
      Indique que la matrice de projection doit être redéfinie */
  bool mWindowChanged;
public:
   /** @brief  Constructeur prenant la géométrie de la fenêtre
   * Initialise les données spécifiques à l'affichage.
   **/
  DisplayManager();

  /** Notifie la vue que la perspective doit être reconstruite avant affichage */
  void setWindowChanged();

  /** @brief Méthode d'affichage des données du modèle
   * @param modele Modèle contenant les données à afficher
   **/
  void Affichage( Modele& modele);

  /** @brief Réglage du cadrage pour la vue
   * Doit être rappelée si la taille de la vue change (SDL_WINDOWEVENT)
   * @param modele Modèle de données contenant les informations sur le viewport
   * (cadre 2D de la vue) et la projection (orthogonale, perspective, etc.)
   */
  void Redimensionnement(const Modele& modele);
};
#endif
