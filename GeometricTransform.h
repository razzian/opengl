#ifndef GEOMETRIC_TRANSFORM_FLMISTRAL
#define GEOMETRIC_TRANSFORM_FLMISTRAL

#include <cstdlib>
#include <GLES3/gl3.h>
#include <GL/glut.h>
#include <vector>

using namespace std;

struct GeometricTransform
{
	static void Viewport(int32_t viewCenterX, int32_t viewCenterY, u_int32_t viewWidth, u_int32_t viewHeight);

	static void ApplyPerspectiveProjection(GLfloat angleOuvertureY, GLfloat aspect, GLfloat zProche, GLfloat zEloigne);

	static void ClearModelView();

	static void ClearProjection();

	static void LookAt(const vector<GLfloat> position, const vector<GLfloat> pointDeVisee, const vector<GLfloat> vecteurVertical);

	static void Translate(GLfloat vecX, GLfloat vecY, GLfloat vecZ);
		
	static void Rotate(GLfloat vecX, GLfloat vecY, GLfloat vecZ, GLfloat angle);

	static void scale(GLfloat factorX, GLfloat factorY, GLfloat factorZ);
};


#endif
