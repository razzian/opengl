#include "GraphicObject.h"

GraphicObject::GraphicObject() : nbSommet(0), nbFace(0), sommets(), faces(), objetGraphiques(), texture(0)
{
}

GraphicObject::~GraphicObject() 
{}

int GraphicObject::getnbFace()
{
    return nbFace;
}

void GraphicObject::setTexture(const char * image)
{
    texture = TextureManager::telechargerTexture(image);
}

void GraphicObject::ajouterSommet(GLfloat x,GLfloat y,GLfloat z)
{
    Sommet tmp(x,y,z);
    sommets.push_back(tmp);

    nbSommet ++;
}

void GraphicObject::ajouterSommet(Sommet s)
{
    sommets.push_back(s);
    nbSommet ++;
}

void GraphicObject::ajouterObjetGraphique(GraphicObject & go)
{
    objetGraphiques.push_back(go);
}

bool GraphicObject::chercherSommet(Sommet s)
{
    // recherche d'un sommet dans une liste
    bool trouve = false;
    unsigned int i = 0;
    while(i<sommets.size() && !trouve)
    {
        trouve = (s == sommets[i]);
        i++;
    }
    return trouve;
}
void GraphicObject::definirFace(Sommet s1, Sommet s2, Sommet s3)
{
    Face face(s1,s2,s3);
    // on ajoute les sommets s'ils ne sont pas presents
    if(!chercherSommet(s1))
    {
        sommets.push_back(s1);
        nbSommet ++;
    }
    if(!chercherSommet(s2))
    {
        sommets.push_back(s2);
        nbSommet ++;
    }
    if(!chercherSommet(s3))
    {
        sommets.push_back(s3);
        nbSommet ++;
    }

    // on ajoute la face
    faces.push_back(face);
    nbFace ++;
}

void GraphicObject::dessiner() const
{
    // on commence par dessiner tout les objects graphiques composant le GraphicObject
    for(unsigned int  j=0 ; j < objetGraphiques.size() ; j ++)
    {
        objetGraphiques[j].dessiner();
    }

    // on dessine avec openGL le GraphicObject
    // on attache la texture à notre objet
    // glBindTexture(GL_TEXTURE_2D, texture);
    // on dessine un triangle pour chaque face de notre GraphicObject
    glBegin(GL_TRIANGLES);
    for(int i = 0; i< nbFace ; i ++)
    {
        //glColor3ub(250,0,0); // on peut applique une couleur à la face
        //glTexCoord2d(0.0,0.0); // on peut associer les coordonnées de la texture aux coordonnées de la face 
        glNormal3f(faces[i].s1.normal[0],faces[i].s1.normal[1],faces[i].s1.normal[2]); // on attache une normal à chaque sommet de la face
        glVertex3f(faces[i].s1.x, faces[i].s1.y, faces[i].s1.z);

        //glTexCoord2d(0.0,1.0);
        glNormal3f(faces[i].s2.normal[0],faces[i].s2.normal[1],faces[i].s2.normal[2]);
        glVertex3f(faces[i].s2.x, faces[i].s2.y, faces[i].s2.z);

        //glTexCoord2d(0.5,1.0);
        glNormal3f(faces[i].s3.normal[0],faces[i].s3.normal[1],faces[i].s3.normal[2]);
        glVertex3f(faces[i].s3.x, faces[i].s3.y, faces[i].s3.z);

    }
    glEnd();   
    glFlush();   
}