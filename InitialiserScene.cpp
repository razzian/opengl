#include "InitialiserScene.h"


GraphicObject InitialiserScene::initDepuisFichier(std::string nomFichier)
{
    GraphicObject renduFinal;																															// la scène

    Assimp::Importer importer;																															// Objet permettant d'importer avec ASSIMP
    const aiScene * sceneAssimp = importer.ReadFile(nomFichier.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);		// scene chargee au format ASSIMP

    if(sceneAssimp)																																		// si la scene s'est chargee
    {
        for(unsigned int i = 0; i < sceneAssimp->mNumMeshes; i++)																						// on parcourt les Mesh
        {
            GraphicObject tmp;																															// pour chaque mesh on creait un objet graphique
            const aiMesh * meshCourant = sceneAssimp->mMeshes[i];																						// on recuepre la mesh courante
            for(unsigned int j = 0; j < meshCourant->mNumFaces; j++)																					// on parcourt les faces de la mesh
            {
                const aiFace& faceCourante = meshCourant->mFaces[j];																					// on recupere la face courante
                assert(faceCourante.mNumIndices == 3);																									// on verifie que c'est bien un triangle
                int indice0 = faceCourante.mIndices[0];																									// on recuepre les indices des sommets associes a la face
                int indice1 = faceCourante.mIndices[1];
                int indice2 = faceCourante.mIndices[2];

                const aiVector3D* sommetAssimp0 = &(meshCourant->mVertices[indice0]);																	// on recuepere les sommets dans la liste des sommets. Il n'y a pas de verification pour savoir si l'indice est correct. On considere qu'il n'y a pas d'erreur dans le fichier
                const aiVector3D* sommetAssimp1 = &(meshCourant->mVertices[indice1]);
                const aiVector3D* sommetAssimp2 = &(meshCourant->mVertices[indice2]);

                const aiVector3D* normalAssimp0 = &(meshCourant->mNormals[indice0]) ;																	// on recuepre les normales aux sommets
                const aiVector3D* normalAssimp1 = &(meshCourant->mNormals[indice1]) ;
                const aiVector3D* normalAssimp2 = &(meshCourant->mNormals[indice2]) ;

                std::vector<GLfloat> normal0{normalAssimp0->x, normalAssimp0->y, normalAssimp0->z};														// on cree un vecteur de GLfloat correspondant a la normal de chaque sommet
                std::vector<GLfloat> normal1{normalAssimp1->x, normalAssimp1->y, normalAssimp1->z};
                std::vector<GLfloat> normal2{normalAssimp2->x, normalAssimp2->y, normalAssimp2->z};

                Sommet s0 (sommetAssimp0->x,sommetAssimp0->y, sommetAssimp0->z, normal0);																// on cree des sommets a partir de ceux obtenus plus tot
                Sommet s1 (sommetAssimp1->x,sommetAssimp1->y, sommetAssimp1->z, normal1);
                Sommet s2 (sommetAssimp2->x,sommetAssimp2->y, sommetAssimp2->z, normal2); 

                tmp.definirFace(s0,s1,s2);																												// on definit une face a partir des sommets
            }

            renduFinal.ajouterObjetGraphique(tmp);																										// on ajoute l'objet graphique a l'objet graphique final
        }
    }

    return renduFinal;
}

GraphicObject InitialiserScene::init()
{
    GraphicObject scene; // la scéne retourné

    // construction de deux pyramides
    GraphicObject pyramide1;
    GraphicObject pyramide2;
    Sommet s1(-2.5,0.0,0.0);
    Sommet s2(-1.5,0.0,0.0);
    Sommet s3(-2.0,0.0,1.0);
    Sommet s4(-2.0,1.0,0.5);

    Sommet s5(1.5,0.0,0.0);
    Sommet s6(2.5,0.0,0.0);
    Sommet s7(2.0,0.0,1.0);
    Sommet s8(2.0,1.0,0.5);

    pyramide1 = dessinerPyramide(s1,s2,s3,s4);
    pyramide2 = dessinerPyramide(s5,s6,s7,s8);

    // on associe les textures aux pyramides
    pyramide1.setTexture("textures/Sky.bmp");
    pyramide2.setTexture("textures/Sky.bmp");

    // on construit notre scène avec les deux pyramides.
    scene.ajouterObjetGraphique(pyramide1);
    scene.ajouterObjetGraphique(pyramide2);

    return scene;
}


GraphicObject InitialiserScene::dessinerTriangle(Sommet & s1, Sommet & s2 , Sommet & s3)
{
    GraphicObject triangle;

    triangle.definirFace(s1,s2,s3);
    return triangle ;
}

GraphicObject InitialiserScene::dessinerPyramide(Sommet & s1, Sommet & s2 , Sommet & s3 , Sommet & s4)
{
    GraphicObject pyramide;

    pyramide.definirFace(s1,s3,s2);
    pyramide.definirFace(s1,s2,s4);
    pyramide.definirFace(s3,s1,s4);
    pyramide.definirFace(s2,s3,s4);

    return pyramide ;
}