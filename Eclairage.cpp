#include "Eclairage.h"



void Eclairage::initEclairageAmbiant()
{
    // lumière ambiante

    GLfloat  ambient_scene [ ] = {1.0 ,  1.0 ,  1.0 ,  1.0} ;
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT,  ambient_scene ) ;

    GLfloat  ambient [ ] = {0.23125f , 0.23125f , 0.23125f , 89.6f } ;
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT,  ambient ) ;

}

void Eclairage::initEclairageDiffus()
{
    // lumière  diffuse
    glEnable(GL_LIGHT0);

    GLfloat lumiereDiffuse0[] = {1.0 , 0.0, 1.0 ,1.0};
    glLightfv (GL_LIGHT0, GL_DIFFUSE, lumiereDiffuse0 );

    GLfloat diffuse[] = { 0.2f ,  0.2f ,  0.2f ,  1.0f} ;
    glMaterialfv (GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse) ;

    // position lumière 0
    GLfloat lightPosition0[] = { 75 , 75 ,0 , 1.0 } ;
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition0) ;
}

void Eclairage::initEclairageSpeculaire()
{
    // lumière speculaire
    glEnable(GL_LIGHT1);

    GLfloat specular_light1[] = { 1.0 , 0.0 , 1.0 , 1.0 };
    glLightfv (GL_LIGHT1, GL_SPECULAR, specular_light1 );

    GLfloat specular[] = {0.2f ,  0.2f ,  0.2f ,  1.0f} ;
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 110.0);

    // position lumière 1
    GLfloat lightPosition1[] = { 75 , 75 ,0 , 1.0 } ;
    glLightfv(GL_LIGHT1, GL_POSITION, lightPosition1) ;
}

/* active la possibilite de faire de l'eclairage */
void Eclairage::activerEclairage()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
}