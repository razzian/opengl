#ifndef EVENT_CONTROLLER_FLMISTRAL
#define EVENT_CONTROLLER_FLMISTRAL

#include <cstdlib>
#include <cstdio>
#include <SDL2/SDL.h>

#include "MouseData.h"
#include "Modele.h"

/** @brief Classe Controleur d'événements utilisateur et timer.
   */
  
class EventController{

  public:
    /** Initialisation du timer et démarrage de la boucle d'événements */
    static void Init(SDL_Window* p_window, Modele* p_modele);

  private:
    /** Type pour représenter les sortes d'événements de type SDL_USEREVENT */
    enum UserEventKinds{
      ANIMATION_TIMER=0
    };

    /**@brief Gestion d'un événement SDL extrait de la file
     * @param p_evenement données de l'événement
     * @param window  Fenêtre SDL (pour gérer les SDL_WINDOWEVENT)
     * @param p_ParamsAffichage instance de la classe Vue
     * @return true si l'événement est SDL_QUIT (fermeture de la fenêtre)
     */
    static bool Handle_SDL_Event(const SDL_Event *p_evenement, SDL_Window *p_window, Modele *p_modele);

    /** Boucle d'attente des événements
     * @param window Fenêtre SDL (pour gérer les SDL_WINDOWEVENT)
     * @param p_ParamsAffichage instance de la classe Vue
     **/
    static void DoEventsLoop(SDL_Window *window, Modele *p_modele);

    /**
     * @brief Callback du timer de raffraichissement de la vue
     * @see http://sdl.beuc.net/sdl.wiki/SDL_AddTimer
     * @param interval intervalle de temps en millisecondes entre deux invocations
     * @param m_modele adresse du modèle de données de l'application castée en void*
     */
    static u_int32_t CreateTimerRefreshFrame(Uint32 interval, void* p_modele);

    /** Identifiant du timer d'annimation */
    static SDL_TimerID mTimerId; // Au cas où on ait besoin de modifier
};

#endif