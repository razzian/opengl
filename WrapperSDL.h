/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: WrapperSDL.h                                                       * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#ifndef HEADER_WRAPPER_SDL_H
#define HEADER_WRAPPER_SDL_H

#include <SDL2/SDL.h>
#include <GLES3/gl3.h>
#include <GL/glut.h>

#include "WindowManager.h"
#include "Modele.h"


/**
 * Classe Wrapper pour SDL et assurant les fonctionnalités de la GUI
 */
class WrapperSDL{

public:
  /** Construit la fenêtre graphique aux dimensions, avec le contexte OpenGL */
  WrapperSDL(u_int32_t largeurFenetreInit, u_int32_t hauteurFenetreInit,
             const char* windowTitle, int argc, char**argv);
  /** @return the number of milliseconds since the SDL library initialized. */
  static u_int32_t getTicks();

  ~WrapperSDL() = default;

private:
  /** Instance de la classe gérant la fenêtre graphique */
  WindowManager mWindowManager;
  /** Instance du modèle de données de l'application */
  Modele mModele;
};
#endif
