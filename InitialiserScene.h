/**
 * \file InitialiserScene.h
 * \brief Fichier d'entête de la classe InitialiserScene
 * \author Falletty Elouan, Floriane Mistral
 * \version 1.0
 * \date 18/01/2019
 *
 * Fichier d'entête de la classe InitialiserScene
 *
 */
#ifndef INITIALISER_SCENE_FLMISTRAL
#define INITIALISER_SCENE_FLMISTRAL

#include <GL/glut.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <string>

#include "Sommet.h"
#include "Face.h"
#include "GraphicObject.h"

/**
 * \class InitialiserScene InitialiserScene.h
 * \brief Class InitialiserScene
 *
 * Classe utiliser pour la construction initial de la scene et de tous les GraphicObject qu'elle possédent
 */
class InitialiserScene
{
  public :

    /**
    * \fn static GraphicObject initDepuisFichier(std::string nomFichier)
    * \brief Construit une scene avec des methodes de la bibliotheque assimp à partir d'un fichier entre en parametre
    *
    * \param nomFichier Un string contenant le nom du fichier de la scène compatible avec assimp
    *
    * \return Un GraphicObject qui est la scene 
    */
    static GraphicObject initDepuisFichier(std::string nomFichier);

    /**
    * \fn static GraphicObject init()
    * \brief Construit une scène définit manuellement (c'est la fonction a modifier pour construire soit meme une scene personnalisee) 
    *
    * \return Un GraphicObject qui est la scène 
    */
    static GraphicObject init();

    /**
    * \fn static GraphicObject dessinerTriangle(Sommet & s1, Sommet & s2, Sommet & s3)
    * \brief Renvoi un GraphicObject representant un triangle qui a pour sommet ceux entres en parametre
    *
    * \param s1 Le premier Sommet du triangle retourne
    * \param s2 Le deuxième Sommet du triangle retourne
    * \param s3 Le troixème Sommet du triangle retourne
    *
    * \return Un GraphicObject représentant le triangle
    */
    static GraphicObject dessinerTriangle(Sommet & s1, Sommet & s2, Sommet & s3);

    /**
    * \fn static GraphicObject dessinerPyramide(Sommet & s1,Sommet & s2, Sommet & s3, Sommet & s4)
    * \brief Renvoi un GraphicObject représentant une pyramide qui a pour sommet ceux entrés en paramètre
    *
    * \param s1 Le premier Sommet de la pyramide retournée
    * \param s2 Le deuxième Sommet de la pyramide retournée
    * \param s3 Le troixème Sommet de la pyramide retournée
    * \param s4 Le quatrième Sommet de la pyramide retournée
    *
    * \return Un GraphicObject représentant la pyramide
    */
    static GraphicObject dessinerPyramide(Sommet & s1,Sommet & s2, Sommet & s3, Sommet & s4);


};

#endif